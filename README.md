
# YShop

## 介绍

- 预览：[Api](http://api.yell.run/index.html)、[Admin](http://admin.yell.run/#/login)  、[Client](http://client.yell.run/)、（更新日期：2021/03/22）
- 注意：服务器配置不好，第一次要慢慢来~
- 使用：[Furion](https://gitee.com/dotnetchina/Furion) 、[Element-UI](https://element.eleme.cn/#/zh-CN) 、[UniApp](https://uniapp.dcloud.io/)
- 包含：代码生成器、权限管理、后台管理、移动端
- YShop.Core 后端(AspNetCore)    开发中...
- YShop.Admin 后台管理(Vue)    开发中...
- YShop.Client 后台管理(UniApp)    开发中...


### 使用说明

`
YShop.Application > applicationsettings.json > DatabaseSettings 配置连接字符串
`

## 流程图

![](./docs/整体流程.png)

## 样图

| ![](./Snipaste_2021-02-24_22-36-11.png) | ![](./Snipaste_2021-01-24_23-23-04.png) |
| --------------------------------------- | --------------------------------------- |
| ![](./Snipaste_2021-01-24_23-23-57.png) | ![](./Snipaste_2021-01-24_23-19-14.png) |

![](./Snipaste_2021-03-04_23-22-36.png) 



# Cent OS 7.6 部署

## 推荐阅读

[[最全操作指南] 在线六个项目全部迁移Linux](https://mp.weixin.qq.com/s/U-qsP9vbH0tWXyfXBMDlTQ)

[使用 Nginx 在 Linux 上托管 ASP.NET Core](https://docs.microsoft.com/zh-cn/aspnet/core/host-and-deploy/linux-nginx?view=aspnetcore-5.0)

[在 CentOS 上安装 .NET SDK 或 .NET 运行时](https://docs.microsoft.com/zh-cn/dotnet/core/install/linux-centos)

## YShop.Core

### 安装宝塔

- nginx 1.8、mysql 8.0
- 域名解析、添加站点

### 安装DotNet SDK

```
sudo rpm -Uvh https://packages.microsoft.com/config/centos/7/packages-microsoft-prod.rpm
```

```
sudo yum install dotnet-sdk-5.0
```

### 安装Git

```
yum -y install git
```

### 克隆项目

```
cd /home/
rm -rf /home/yshop/
git clone https://gitee.com/yell-run/yshop.git
```

### 编译、发布、尝试启动
```
YShop.Application > applicationsettings.json > DatabaseSettings 配置连接字符串
YShop.Application > hosting.json > urls 配置URL 默认：http://localhost:5000;https://localhost:5001

cd /home/yshop/YShop.Core/YShop.Web.Entry/
dotnet build
dotnet publish -o /home/yshop/YShop.Core/YShop.Web.Entry/bin/Debug/net5.0
rm -rf /home/yshop.bin/
cp -r /home/yshop/YShop.Core/YShop.Web.Entry/bin/Debug/net5.0 /home/yshop.bin/
cd /home/yshop.bin/
chmod 777 YShop.Web.Entry.dll
dotnet YShop.Web.Entry.dll

此时查看启动端口号应该是 5000/5001 或者 采用 hosting.json 中的配置
```

### 守护进程

#### 也可以使用官方自带的，这边采用 PM2

#### 安装node
##### 这边建议源码安装
```
wget https://nodejs.org/dist/v14.16.0/node-v14.16.0-linux-x64.tar.xz
tar -xf node-v14.16.0-linux-x64.tar.xz
vim /etc/profile
PATH=$PATH:/home/node-v14.16.0-linux-x64/bin/ 
source /etc/profile
node -v
npm -v
```

#### 安装 cnpm

```
npm install -g cnpm --registry=https://registry.npm.taobao.org
cnpm -v
```

#### 安装PM2

```
cnpm install pm2 -g
pm2 list
```

#### 启动程序

```
这时候要先关掉 API 项目
pm2 stop all
pm2 delete all
cd /home/yshop.bin/
pm2 start "dotnet YShop.Web.Entry.dll" --name api.yshop 
pm2 list
```

#### 用宝塔建站

##### 配置网站反向代理为启动端口号即可 （当然也可以手动配置，只不过我很懒！）

![](./core_process.png)

## YShop.Admin

### 直接部署静态页就行 

### 可以在本地或服务器编译

```
npm run build
```

![](./admin_process.png)
