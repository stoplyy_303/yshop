import request from '@/utils/request'
import _ from 'lodash'
import lscache from 'lscache'


const apiList = require.context('./', true, /\.js$/)
var api = apiList.keys().reduce((api, path) => {
    const apiNameSplits = path.replace(/^\.\/(.*)\.\w+$/, '$1').split('/')
    var apis = apiList(path)
    for (let f in apis) {
        if (typeof apis[f] == 'function') {
            _.set(api, apiNameSplits.concat(f), apis[f])
        }
    }
    return api
}, {})

api.get = function (url, data) {
    return request({
        url: url,
        method: 'get',
        data: data
    })
}
api.post = function (url, data) {
    return request({
        url: url,
        method: 'post',
        data: data
    })
}

$y['api'] = api

