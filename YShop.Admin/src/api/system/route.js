import request from '@/utils/request'

export function getAll(){
    return request({
        url: '/api/route/all',
        method: 'get'
    })
}

export function getAccessRoutes(token){
    return request({
        url: '/api/route/access/system',
        method: 'get'
    })
}