import request from '@/utils/request'


/**
 * 获取有序的GUID
 */
export function get(){
    return request({
        url: '/api/develop/guid',
        method: 'get'
    })
}