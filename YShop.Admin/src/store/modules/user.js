import { login } from '@/api/system/user.js'
import lscache from 'lscache'
import { getToken, setToken } from '@/utils/auth.js'


const clientSetting = {
	grant_type: 'password',
	scope: 'Vue',
	username: '',
	password: '',
	client_id: 'Vue_App',
	client_secret: '1q2w3e*'
}

const state = {
	token: getToken(),
	rule: 'admin',
	dept: 'IT',
	post: 'IT Principal',
}
const mutations = {
	setToken: (state, token) => {
		state.token = token
	}
}
const actions = {
	login: async({ commit }, userInfo) => {
		userInfo = $y._.assign(clientSetting, userInfo)
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}