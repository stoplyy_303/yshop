import Vue from 'vue'


Vue.mixin({
    beforeRouteLeave(to,from,next){
        if(!this.$store.state.tabs.tabs.find(t=>t.path == from.path)){
            this.$destroy()
        }
        next()
    }
})