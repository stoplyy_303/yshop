import lscache from 'lscache'

export function getToken() {
    return lscache.get('token')
}

export function setToken(token, time) {
    return lscache.set('token', token, time)
}