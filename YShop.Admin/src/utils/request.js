import store from '../store';
import axios from "axios";
import qs from 'querystring';


const requestOptions = axios.create({
  baseURL: store.state.base.baseURL,// 'http://api.yell.run/', //process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 60 * 1000, // request timeout
});

/**
 * 请求拦截器
 */
requestOptions.interceptors.request.use(
  (config) => {
    // 开始进度条
    $y.progress.start();
    // do something before request is sent
    // config.headers["accept-language"] = store.getters.language;

    // if (store.getters.token) {
    //   config.headers["authorization"] = "Bearer " + store.getters.token;
    // }

    // if (store.getters.tenant) {
    //   config.headers["__tenant"] = store.getters.tenant;
    // }

    // config.paramsSerializer = function(params) {
    //   return encodeParam(params);
    // };

    return config;
  },
  (error) => {
    // do something with request error
    $y.notifier.error(error)
    return Promise.reject(error);
  }
);

/**
 * 响应拦截器
 */
requestOptions.interceptors.response.use(
  (response) => {
    $y.progress.done();
    const res = response.data;
    return res;
  },
  (error) => {
    $y.progress.done();
    $y.notifier.error(error)
    return Promise.reject(error)
  }
);

export default function request(options) {
  var { method, headers, data } = options
  // if (method.toLowerCase() == 'post') {
  //   $y._.assign(options.headers, { 'content-type': 'application/x-www-form-urlencoded' })
  //   if (data) {
  //     $y._.assign(options.data, qs.stringify(data))
  //   }
  // }
  return requestOptions(options)
};
