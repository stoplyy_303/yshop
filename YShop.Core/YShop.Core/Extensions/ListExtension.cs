﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Mapster;


namespace YShop.Core
{
    public static class ListExtension
    {
        /// <summary>
        /// 分页
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static PagedList<TEntity> ToPageList<TEntity>(this List<TEntity> source,int index,int size) where TEntity : class,new()
        {
            return new PagedList<TEntity> 
            {
                PageIndex = index,
                PageSize = size,
                TotalCount = source.Count,
                TotalPages = source.Count / size + 1,
                HasPrevPages = index < 1,
                HasNextPages = index < source.Count / size,
                Items = source.Skip((index - 1) * size).Take(size)
            };
        }
        public static PagedList<TEntity> ToPageList<TEntity>(this IEnumerable<TEntity> source, int index, int size) where TEntity : class, new()
        => ToPageList(source.ToList(), index, size);
        public static Task<PagedList<TEntity>> ToPageListAsync<TEntity>(this List<TEntity> source, int index, int size) where TEntity : class, new()
        => Task.FromResult(ToPageList(source, index, size));
        public static Task<PagedList<TEntity>> ToPageListAsync<TEntity>(this IEnumerable<TEntity> source, int index, int size) where TEntity : class, new()
        => Task.FromResult(ToPageList(source, index, size));

        /// <summary>
        /// 带查询的分页
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="source"></param>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public static PagedList<TResult> ToPageList<TEntity,TResult>(this List<TEntity> source, int index, int size, ExpressionBuilder<TResult> query) 
            where TEntity : class, new()
            where TResult : class,new()
        {
            var results = source.Adapt<List<TResult>>();
            if (query != null && query.Result() != null)
                results = results.Where(query.Compile()).ToList();
            return new PagedList<TResult>
            {
                PageIndex = index,
                PageSize = size,
                TotalCount = results.Count,
                TotalPages = results.Count / size + 1,
                HasPrevPages = index < 1,
                HasNextPages = index < results.Count / size,
                Items = results.Skip((index - 1) * size).Take(size)
            };
        }
        public static Task<PagedList<TResult>> ToPageListAsync<TEntity, TResult>(this List<TEntity> source, int index, int size, ExpressionBuilder<TResult> query)
            where TEntity : class, new()
            where TResult : class, new()
            => Task.FromResult(ToPageList(source, index, size, query));
        public static Task<PagedList<TResult>> ToPageListAsync<TEntity, TResult>(this IEnumerable<TEntity> source, int index, int size, ExpressionBuilder<TResult> query)
            where TEntity : class, new()
            where TResult : class, new()
            => Task.FromResult(ToPageList(source, index, size, query));
        public static PagedList<TResult> ToPageList<TEntity, TResult>(this IEnumerable<TEntity> source, int index, int size, ExpressionBuilder<TResult> query)
            where TEntity : class, new()
            where TResult : class, new()
            => ToPageList(source, index, size, query);
    }
}
