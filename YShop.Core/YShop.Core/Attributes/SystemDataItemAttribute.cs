﻿using System;
using YShop.Core.System;

namespace YShop.Core
{
    /// <summary>
    /// 系统数据项标记
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SystemDataItemAttribute: Attribute
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public DataItemDetailsDataType Type { get; set; }
        /// <summary>
        /// 异步
        /// </summary>
        public bool IsAync { get; set; }
        /// <summary>
        /// 系统数据项标记
        /// </summary>
        /// <param name="code">获取代码</param>
        /// <param name="isAync">是否异步</param>
        /// <param name="type">数据项类型</param>
        public SystemDataItemAttribute(string code, bool isAync = false, DataItemDetailsDataType type = DataItemDetailsDataType.List)
        {
            Code = code;
            Type = type;
            IsAync = isAync;
        }
    }
}
