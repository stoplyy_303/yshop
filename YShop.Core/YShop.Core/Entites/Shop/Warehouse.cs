﻿namespace YShop.Core.Entites
{
    /// <summary>
    /// 仓库
    /// </summary>
    public class Warehouse: BaseEntity
    {
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 负责人（编号）
        /// </summary>
        public string Principal { get; set; }
    }

    /// <summary>
    /// 库位
    /// </summary>
    public class WarehouseLocation : BaseEntity
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string Code { get; set; }
    }
}
