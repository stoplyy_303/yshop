﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.Entites
{
    /// <summary>
    /// 支付记录
    /// </summary>
    public class Payment : BaseEntity
    {
        /// <summary>
        /// 支付编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 支付类型
        /// JSAPI,App,H5...
        /// </summary>
        public PayType Type { get; set; }
        /// <summary>
        /// 支付方式
        /// WeChat,Alipay...
        /// </summary>
        public PayMode Mode { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime PayTime { get; set; }
        /// <summary>
        /// 支付状态
        /// </summary>
        public PayState State { get; set; }
        /// <summary>
        /// 状态描述
        /// </summary>
        public string StateDesc { get; set; }
        /// <summary>
        /// 货币
        /// </summary>
        public Currency Currency { get; set; }
        /// <summary>
        /// 外部单号
        /// </summary>
        public string OuterCode { get; set; }
        /// <summary>
        /// 商户号
        /// </summary>
        public string Mchid { get; set; }
        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// 支付者Id
        /// </summary>
        public string OpenId { get; set; }

    }
    /// <summary>
    /// 支付状态
    /// </summary>
    public enum PayState
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success,
        /// <summary>
        /// 转入退款
        /// </summary>
        Refund,
        /// <summary>
        /// 未支付
        /// </summary>
        NotPay, 
        /// <summary>
        /// 已关闭
        /// </summary>
        Closed,
        /// <summary>
        /// 已撤销
        /// </summary>
        Revoked, 
        /// <summary>
        /// 用户支付中
        /// </summary>
        UserPaying,
        /// <summary>
        /// 支付失败
        /// </summary>
        PayError
    }
    /// <summary>
    /// 货币
    /// </summary>
    public enum Currency
    {
        /// <summary>
        /// 人命币
        /// </summary>
        CHY, 
        /// <summary>
        /// 澳门元
        /// </summary>
        MOP, 
        /// <summary>
        /// 港币
        /// </summary>
        HKD, 
        /// <summary>
        /// 欧元
        /// </summary>
        EUR,
        /// <summary>
        /// 美元
        /// </summary>
        USD
    }
    /// <summary>
    /// 支付类型
    /// </summary>
    public enum PayType 
    {
        JSAPI,
        APP,
        H5,
        Native,
        Applet,
        ScanCode, 
        FacePay
    }
    /// <summary>
    /// 支付方式
    /// </summary>
    public enum PayMode
    {
        /// <summary>
        /// 微信
        /// </summary>
        WeChat, 
        /// <summary>
        /// 支付宝
        /// </summary>
        Alipay
    }
}
