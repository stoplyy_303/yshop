﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.Entites
{
    public class Logistics:BaseEntity
    {

        /// <summary>
        /// 物流编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 物流状态
        /// </summary>
        public LogisticsState State { get; set; }
        /// <summary>
        /// 物流供应商
        /// </summary>
        public string Supplier { get; set; }

    }

    public enum LogisticsState
    {

    }
}
