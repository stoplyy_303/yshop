﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.Entites.Shop
{
    public class Customer : BaseEntity
    {
        public string Name { get; set; }

        public string OpenId { get; set; }

        public string PhoneNumber { get; set; }
    }
}
