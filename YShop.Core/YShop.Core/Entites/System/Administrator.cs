﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.Entites.System
{
    /// <summary>
    /// 管理员
    /// </summary>
    [Table("Sys_Administrator")]
    public class Administrator:BaseEntity
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 职位
        /// </summary>
        public string PositionId { get; set; }
        public Position Position { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public string DepartmentId { get; set; }
        public Department Department { get; set; }
        /// <summary>
        /// 公司Id
        /// </summary>
        public string CompanyId { get; set; }
        public Company Company { get; set; }
    }
}
