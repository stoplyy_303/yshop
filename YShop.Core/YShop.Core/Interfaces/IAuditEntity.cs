﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core
{
    public interface IAuditEntity
    {
        /// <summary>
        /// 审核人编号
        /// </summary>
        public string AuditUserCode { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? AuditTime { get; set; }
        /// <summary>
        /// 是否审核
        /// </summary>
        public bool IsAudit { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public AuditState AuditState { get; set; }
    }
}
