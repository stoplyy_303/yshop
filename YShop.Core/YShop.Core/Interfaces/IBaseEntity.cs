﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;

namespace YShop.Core
{
    public interface IBaseEntity: IEntity
    {
        /// <summary>
        /// 实体主键
        /// </summary>
        [Key]
        [MaxLength(36)]
        public string Id { get; set; } 
    }
}
