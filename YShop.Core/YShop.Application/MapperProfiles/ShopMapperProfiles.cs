﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mapster;
using Newtonsoft.Json;
using YShop.Application.Contracts.Shop;
using YShop.Application.Contracts.System.Resources;
using YShop.Core.Entites;
using YShop.Core.Entites.Shop;
using YShop.Core.System;

namespace YShop.Application.MapperProfiles
{
    public class ShopMapperProfiles : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            //config.ForType<Product, ProductDto>()
            //    .Map(d => d.Options, s => JsonConvert.DeserializeObject<List<ProductOptionDto>>(s.Options));
            //config.ForType<ProductDto, Product>()
            //    .Map(d => d.Options, s => JsonConvert.SerializeObject(s.Options));

            #region 商品
            config.ForType<Product, ProductDto>()
                .Map(d => d.Images, s => string.IsNullOrEmpty(s.Images) ? new List<ResourceDto>() : JsonConvert.DeserializeObject<List<ResourceDto>>(s.Images));
            config.ForType<ProductDto, Product>()
                .Map(d => d.Images, s => JsonConvert.SerializeObject(s.Images));

            config.ForType<ProductDetailDto, ProductDetail>()
                .Map(d => d.Options, s => JsonConvert.SerializeObject(s.Options));

            config.ForType<ProductDetail, ProductDetailDto>()
                .Map(d => d.Options, s => string.IsNullOrEmpty(s.Options) ? new List<string>() : JsonConvert.DeserializeObject(s.Options));
            #endregion

            #region 商品分类
            #endregion

        }
    }
}
