﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;
using Microsoft.AspNetCore.Mvc;
using YShop.Application.Contracts.Shop;
using YShop.Application.Contracts.System.DataItems;
using YShop.Core;
using YShop.Core.Entites.Shop;
using YShop.Core.Managers.Caches;

namespace YShop.Application.Shop
{
    /// <summary>
    /// 商品分类
    /// </summary>
    [ApiDescriptionSettings("Shop")]
    public class ProductCategroyService 
        : CrudService<ProductCategroy, ProductCategroyDto, ProductCategroyDto, PageSearchDto, ProductCategroyDto, ProductCategroyDto>,IBaseService
    {
        private readonly IRepository<ProductCategroy> _repository;
        private readonly CacheManager _cacheManager;

        public override List<ProductCategroy> EntitiesCache => _cacheManager.GetOrCreate(CacheKeys.ProductCategroys, _ => 
        {
            return _repository.AsEnumerable(false).ToList();
        });
        public override string EntitiesCacheKey => CacheKeys.ProductCategroys;
        public ProductCategroyService(IRepository<ProductCategroy> repository,CacheManager cacheManager) : base(repository)
        {
            _repository = repository;
            _cacheManager = cacheManager;
        }

        /// <summary>
        /// 获取数据项
        /// </summary>
        /// <returns></returns>
        [SystemDataItem(code: "ProductCategroy", type: DataItemDetailsDataType.List)]
        public List<DataItemDetailDto> GetItems() => EntitiesCache.Select(s =>
                                                               new DataItemDetailDto
                                                               {
                                                                   Value = s.Id,
                                                                   Label = s.Name
                                                               }).ToList();
    }
}
