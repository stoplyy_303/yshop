﻿using System.Collections.Generic;
using System.Threading.Tasks;
using YShop.Core;

namespace YShop
{
    /// <summary>
    /// CRUD应用服务接口
    /// </summary>
    public interface ICrudService<TEntityDto, SearchDto, PageSearchDto, CreateDto, UpdateDto>
        where TEntityDto : new()
    {
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TEntityDto> GetAsync(string id);

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="pageSearchDto"></param>
        /// <returns></returns>

        Task<PagedList<TEntityDto>> GetListAsync(PageSearchDto pageSearchDto);
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="create"></param>
        /// <returns></returns>
        Task CreateAsync(List<CreateDto> create);
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        Task UpdateAsync(List<UpdateDto> update);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="keyJoin"></param>
        /// <returns></returns>
        Task DeleteAsync(string keyJoin);
    }
}
