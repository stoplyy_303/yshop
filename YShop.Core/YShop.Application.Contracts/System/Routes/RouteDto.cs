﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YShop.Core.System;

namespace YShop.Application.Contracts.System.Routes
{
    /// <summary>
    /// 路由
    /// </summary>
    public class RouteDto:BaseEntityDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 重定向
        /// </summary>
        public string Redirect { get; set; }
        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 元信息
        /// </summary>
        public Meta Meta { get; set; }
    }
}
