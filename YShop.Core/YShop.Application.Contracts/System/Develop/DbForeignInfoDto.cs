﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.AgileDevelop.DatabaseManagement
{ 
    public class DbForeignInfoDto
    {
        public DaTableInfoDto Table { get; set; }
        public List<DbColumnInfoDto> Columns { get; set; }
        public DaTableInfoDto ReferencedTable { get; set; }
        public List<DbColumnInfoDto> ReferencedColumns { get; set; }
    }
}
