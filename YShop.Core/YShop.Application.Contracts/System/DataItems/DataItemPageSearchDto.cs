﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.System.DataItems
{
    public class DataItemPageSearchDto : PageSearchDto
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public string CategoryId { get; set; }
    }
}
