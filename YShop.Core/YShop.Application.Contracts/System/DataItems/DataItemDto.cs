﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YShop.Core;
using YShop.Core.System;

namespace YShop.Application.Contracts.System.DataItems
{
    public class DataItemDto:BaseEntityDto
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public string CategoryId { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public DataItemType Type { get; set; }
        /// <summary>
        /// 允许修改
        /// </summary>
        public bool EnableChanges { get; set; } = true;
        /// <summary>
        /// 明细项数据类型
        /// </summary>
        public DataItemDetailsDataType? DetailsDataType { get; set; }
        /// <summary>
        /// 明细项
        /// </summary>
        public dynamic Details { get; set; }
    }
}
