﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.System.DataItems
{
    public class DataItemCategoryHasChildrenDto : DataItemCategoryDto
    {
        /// <summary>
        /// 子项
        /// </summary>
        public List<DataItemCategoryHasChildrenDto> Children { get; set; }
        /// <summary>
        /// 是否有子项
        /// </summary>
        public bool HasChild { get; set; }

        public void AddChildren(DataItemCategoryHasChildrenDto childrenDto)
        {
            this.HasChild = true;
            this.Children ??= new List<DataItemCategoryHasChildrenDto>();
            this.Children.Add(childrenDto);
        }
    }
}
