﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts
{
    public class BaseEntityDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string Id { get; set; } 
    }
}
