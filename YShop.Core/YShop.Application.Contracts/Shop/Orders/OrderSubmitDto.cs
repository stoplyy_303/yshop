﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.Shop.Orders
{
    /// <summary>
    /// 订单提交输入DTO
    /// </summary>
    public class OrderSubmitInputDto
    {
        /// <summary>
        /// 订单明细
        /// </summary>
        public List<OrderSubmitInputItemDto> Items { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }
    }


    /// <summary>
    /// 订单提交输入明细项
    /// </summary>
    public class OrderSubmitInputItemDto
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// SkuId
        /// </summary>
        public string ProductDetailId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

    }
}
