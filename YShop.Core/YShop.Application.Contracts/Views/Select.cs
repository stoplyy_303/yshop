﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.Views
{
    /// <summary>
    /// 下拉框
    /// </summary>
    public class Select<TSource>
    {
        /// <summary>
        /// 显示名
        /// </summary>
        public string Laebl { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public TSource Value { get; set; }
    }
}
