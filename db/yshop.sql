/*
 Navicat Premium Data Transfer

 Source Server         : YShop
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : 8.129.126.230:3306
 Source Schema         : yshop

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 23/01/2021 20:59:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `ProductDetailCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SKU 编号',
  `ProductDetailId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SKU Id',
  `Quantity` decimal(10, 2) NOT NULL COMMENT '数量',
  `Warehouse` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库（用编号）',
  `Location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库位（用编号）',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventory
-- ----------------------------

-- ----------------------------
-- Table structure for logistics
-- ----------------------------
DROP TABLE IF EXISTS `logistics`;
CREATE TABLE `logistics`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流编码',
  `State` int NOT NULL COMMENT '物流状态',
  `Supplier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流供应商',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logistics
-- ----------------------------

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付编号',
  `Type` int NOT NULL COMMENT '支付类型\r\n            JSAPI,App,H5...',
  `Mode` int NOT NULL COMMENT '支付方式\r\n            WeChat,Alipay...',
  `PayTime` datetime(3) NOT NULL COMMENT '支付时间',
  `State` int NOT NULL COMMENT '支付状态',
  `StateDesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态描述',
  `Currency` int NOT NULL COMMENT '货币',
  `OuterCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部单号',
  `Mchid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商户号',
  `AppId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AppId',
  `OpenId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付者Id',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment
-- ----------------------------

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编号',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `SubName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子名称',
  `Saleable` bit(1) NOT NULL COMMENT '上架',
  `Options` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '产品选项\r\n            示例：{颜色:[黑、红、绿],尺寸:[S、M、L、XL、XXL]}',
  `Describe` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '产品描述（富文本）',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品（SUP）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------

-- ----------------------------
-- Table structure for productcategroy
-- ----------------------------
DROP TABLE IF EXISTS `productcategroy`;
CREATE TABLE `productcategroy`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品分类名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productcategroy
-- ----------------------------

-- ----------------------------
-- Table structure for productdetail
-- ----------------------------
DROP TABLE IF EXISTS `productdetail`;
CREATE TABLE `productdetail`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SKU编号',
  `Price` decimal(10, 2) NOT NULL COMMENT '单价',
  `InsidePrice` decimal(10, 2) NOT NULL COMMENT '内部价',
  `Images` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片（如果没有，则默认使用主图）',
  `SpecIndex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格下标（0,0=&gt;黑,S）（0,1=&gt;黑,M）',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品明细（SKU）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productdetail
-- ----------------------------

-- ----------------------------
-- Table structure for salesorder
-- ----------------------------
DROP TABLE IF EXISTS `salesorder`;
CREATE TABLE `salesorder`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '销售订单编号',
  `Source` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源（wechat、h5、web...）',
  `AuditUserCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核人编号',
  `AuditTime` datetime(3) NULL DEFAULT NULL COMMENT '审核时间',
  `IsAudit` bit(1) NOT NULL COMMENT '是否审核',
  `AuditState` int NOT NULL COMMENT '审核状态',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of salesorder
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dataitem
-- ----------------------------
DROP TABLE IF EXISTS `sys_dataitem`;
CREATE TABLE `sys_dataitem`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `CategoryId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类ID',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Type` int NOT NULL COMMENT '类型',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据项' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dataitem
-- ----------------------------
INSERT INTO `sys_dataitem` VALUES ('5ff4ded3-2999-3150-0090-c54609c792e1', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '5ff4ddc4-2999-3150-0090-c54458c0c811', 'MenuType', '菜单类型', 0);
INSERT INTO `sys_dataitem` VALUES ('5ff4df02-2999-3150-0090-c54743ae8369', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '5ff4ddc4-2999-3150-0090-c54458c0c811', 'DataItemType', '数据项类型', 0);

-- ----------------------------
-- Table structure for sys_dataitemcategory
-- ----------------------------
DROP TABLE IF EXISTS `sys_dataitemcategory`;
CREATE TABLE `sys_dataitemcategory`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `ParentId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级Id\r\n            最顶级的父Id为 \'0\'',
  `Lft` int NOT NULL COMMENT '左节点',
  `Rgt` int NOT NULL COMMENT '右节点',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据项分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dataitemcategory
-- ----------------------------
INSERT INTO `sys_dataitemcategory` VALUES ('5ff4dd74-2999-3150-0090-c54313cdb591', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '系统管理', '0', 0, 5);
INSERT INTO `sys_dataitemcategory` VALUES ('5ff4ddc4-2999-3150-0090-c54458c0c811', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '功能', '5ff4dd74-2999-3150-0090-c54313cdb591', 1, 2);
INSERT INTO `sys_dataitemcategory` VALUES ('5ff4de5d-2999-3150-0090-c545721b25f4', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '设置', '5ff4dd74-2999-3150-0090-c54313cdb591', 3, 4);

-- ----------------------------
-- Table structure for sys_dataitemdetail
-- ----------------------------
DROP TABLE IF EXISTS `sys_dataitemdetail`;
CREATE TABLE `sys_dataitemdetail`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `DataItemId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据项Id',
  `Label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显式名',
  `Value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隐式值',
  `ShortName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据项明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dataitemdetail
-- ----------------------------
INSERT INTO `sys_dataitemdetail` VALUES ('5ff4df67-2999-3150-0090-c5482ae88786', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '5ff4ded3-2999-3150-0090-c54609c792e1', '根菜单', '0', 'GCD');
INSERT INTO `sys_dataitemdetail` VALUES ('5ff4df71-2999-3150-0090-c5491c774728', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '5ff4ded3-2999-3150-0090-c54609c792e1', '二级菜单', '1', 'EJCD');
INSERT INTO `sys_dataitemdetail` VALUES ('5ff4df78-2999-3150-0090-c54a3d341ff1', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '5ff4df02-2999-3150-0090-c54743ae8369', '字典', '0', 'ZD');
INSERT INTO `sys_dataitemdetail` VALUES ('5ff4df7f-2999-3150-0090-c54b3b02b4e0', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '5ff4df02-2999-3150-0090-c54743ae8369', '数据源', '1', 'SJY');
INSERT INTO `sys_dataitemdetail` VALUES ('5ff4dff2-2999-3150-0090-c54c769c38b5', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '5ff4ded3-2999-3150-0090-c54609c792e1', '视图菜单', '2', 'STCD');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `Type` int NULL DEFAULT NULL COMMENT '菜单类型\r\n            Root 根菜单\r\n            Group 二级菜单、这里是作为分组\r\n            View 视图菜单',
  `ParentId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级Id\r\n            最顶级的父Id为 \'0\'',
  `RouteId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由外键',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('4bfadb29-a74e-4d81-acb8-d1a90dae9487', NULL, '2021-01-23 20:38:36.740', b'1', b'0', NULL, NULL, NULL, NULL, '商城管理', NULL, 0, '0', NULL);
INSERT INTO `sys_menu` VALUES ('5fefa999-e659-9c58-005c-4f70414fd40c', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '系统管理', NULL, 0, '0', NULL);
INSERT INTO `sys_menu` VALUES ('5fefa9e1-e659-9c58-005c-4f715d544b6f', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '功能', NULL, 1, '5fefa999-e659-9c58-005c-4f70414fd40c', NULL);
INSERT INTO `sys_menu` VALUES ('5fefa9f5-e659-9c58-005c-4f7275f0545e', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '菜单管理', NULL, 2, '5fefa9e1-e659-9c58-005c-4f715d544b6f', '5ff05630-b33d-a7c8-0014-effb6f4d89ab');
INSERT INTO `sys_menu` VALUES ('5ff0d897-b216-a6cc-0000-177979a1dda4', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '示例', NULL, 0, '0', '5ff0c7fd-c953-f494-00b4-329b51a095c2');
INSERT INTO `sys_menu` VALUES ('60044058-c58e-7878-0006-1d8f6919f09a', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '数据管理', NULL, 2, '5fefa9e1-e659-9c58-005c-4f715d544b6f', '5ff23e0e-8f52-f338-00b5-894b4613aac7');
INSERT INTO `sys_menu` VALUES ('600460df-cdbb-986c-00dd-53226ab1fdec', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '敏捷开发', NULL, 0, '0', NULL);
INSERT INTO `sys_menu` VALUES ('6004ae7f-436d-2cac-00f3-016e08619bd1', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '功能', NULL, 0, '600460df-cdbb-986c-00dd-53226ab1fdec', NULL);
INSERT INTO `sys_menu` VALUES ('6004af25-436d-2cac-00f3-01706ee73208', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '代码生成器', NULL, 0, '6004ae7f-436d-2cac-00f3-016e08619bd1', '6004ad61-436d-2cac-00f3-016d2eeb451a');
INSERT INTO `sys_menu` VALUES ('6004af55-436d-2cac-00f3-01714b1cf0ee', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '数据库管理', NULL, 0, '6004ae7f-436d-2cac-00f3-016e08619bd1', '6004aefe-436d-2cac-00f3-016f0bae52ef');
INSERT INTO `sys_menu` VALUES ('9549a6d5-1452-45fb-852f-77a2a08fd72f', NULL, '2021-01-23 20:39:39.145', b'1', b'0', NULL, NULL, NULL, NULL, '数据', NULL, 1, '4bfadb29-a74e-4d81-acb8-d1a90dae9487', NULL);

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Extension` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展名',
  `Path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `Type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `Size` bigint NOT NULL COMMENT '大小',
  `ResourceDirectoryId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目录Id',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------

-- ----------------------------
-- Table structure for sys_resourcedirectory
-- ----------------------------
DROP TABLE IF EXISTS `sys_resourcedirectory`;
CREATE TABLE `sys_resourcedirectory`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `IsPublic` bit(1) NOT NULL COMMENT '公开',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源目录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_resourcedirectory
-- ----------------------------

-- ----------------------------
-- Table structure for sys_route
-- ----------------------------
DROP TABLE IF EXISTS `sys_route`;
CREATE TABLE `sys_route`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `Redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重定向',
  `Component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件',
  `Meta_KeepAlive` bit(1) NULL DEFAULT NULL COMMENT '保持生机（缓存标志）',
  `Meta_Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '路由' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_route
-- ----------------------------
INSERT INTO `sys_route` VALUES ('0', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, 'Home', '/', 'desktop', 'Home', NULL, NULL);
INSERT INTO `sys_route` VALUES ('05518ac4-a5b1-4fc2-bd5a-fedf6819ea12', NULL, '2021-01-23 20:38:37.257', b'1', b'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_route` VALUES ('2b104dd6-b32e-4ccd-b6cd-cf984a3330c5', NULL, '2021-01-23 20:39:39.311', b'1', b'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_route` VALUES ('5ff05630-b33d-a7c8-0014-effb6f4d89ab', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '菜单', '/menu', NULL, 'system/menu/index', b'1', '菜单');
INSERT INTO `sys_route` VALUES ('5ff0c60b-7e5e-4fc8-009b-23a92e481348', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, NULL, '/menu/form', NULL, 'system/menu/form', NULL, NULL);
INSERT INTO `sys_route` VALUES ('5ff0c7ee-c953-f494-00b4-329a422c0eee', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '桌面', '/desktop', NULL, 'system/desktop', NULL, NULL);
INSERT INTO `sys_route` VALUES ('5ff0c7fd-c953-f494-00b4-329b51a095c2', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, NULL, '/sample', NULL, 'sample/index', b'1', '示例');
INSERT INTO `sys_route` VALUES ('5ff0c80d-c953-f494-00b4-329c6acae8cf', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '示例Form', '/sample/form', NULL, 'system/form', NULL, NULL);
INSERT INTO `sys_route` VALUES ('5ff23e0e-8f52-f338-00b5-894b4613aac7', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '数据', '/data', NULL, 'system/data/index', b'1', '数据');
INSERT INTO `sys_route` VALUES ('5ff23e2c-8f52-f338-00b5-894c7c07e767', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '数据Form', '/data/form', NULL, 'system/data/form', NULL, NULL);
INSERT INTO `sys_route` VALUES ('6004ad61-436d-2cac-00f3-016d2eeb451a', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '代码生成器', '/development/codeGenerator', NULL, 'development/codeGenerator/index', b'1', '代码生成器');
INSERT INTO `sys_route` VALUES ('6004aefe-436d-2cac-00f3-016f0bae52ef', NULL, NULL, b'1', b'0', NULL, NULL, NULL, NULL, '数据库管理', '/development/database', NULL, 'development/database/index', b'1', '数据库管理');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Account` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账户',
  `RealName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `NickName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '呢称',
  `HeadIcon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `Gender` int NOT NULL COMMENT '性别',
  `Birthday` datetime(3) NULL DEFAULT NULL COMMENT '生日',
  `MobilePhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `WeChat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信',
  `QQ` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ',
  `Country` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '国家',
  `Province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份',
  `City` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `District` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地区',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------

-- ----------------------------
-- Table structure for warehouse
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库编号',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库名称',
  `Principal` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人（编号）',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of warehouse
-- ----------------------------

-- ----------------------------
-- Table structure for warehouseaccess
-- ----------------------------
DROP TABLE IF EXISTS `warehouseaccess`;
CREATE TABLE `warehouseaccess`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出库编号',
  `Type` int NOT NULL COMMENT '出入库类型',
  `WarehouseCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库编号',
  `SourceCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源编号',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出入库' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of warehouseaccess
-- ----------------------------

-- ----------------------------
-- Table structure for warehouseaccessdetail
-- ----------------------------
DROP TABLE IF EXISTS `warehouseaccessdetail`;
CREATE TABLE `warehouseaccessdetail`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `ProductDetailId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SKUId',
  `ProductDetailCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SKU码',
  `Quantity` decimal(10, 2) NOT NULL COMMENT '数量',
  `Location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库位',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出入库明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of warehouseaccessdetail
-- ----------------------------

-- ----------------------------
-- Table structure for warehouselocation
-- ----------------------------
DROP TABLE IF EXISTS `warehouselocation`;
CREATE TABLE `warehouselocation`  (
  `Id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `CreateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建用户主键',
  `CreateTime` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `IsEnabled` bit(1) NOT NULL COMMENT '是否启用',
  `IsDeleted` bit(1) NOT NULL COMMENT '是否删除',
  `DeleteUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除用户主键',
  `DeleteTime` datetime(3) NULL DEFAULT NULL COMMENT '删除时间',
  `UpdateUserId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改用户主键',
  `UpdateTime` datetime(3) NULL DEFAULT NULL COMMENT '修改时间',
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库位编号',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of warehouselocation
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
